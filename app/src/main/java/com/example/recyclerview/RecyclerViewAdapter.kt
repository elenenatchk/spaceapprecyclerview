package com.example.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.databinding.ItemsLayoutBinding

class RecyclerViewAdapter(private val items: MutableList<itemsModel>): RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>(){
    inner class ItemViewHolder(private val binding:  ItemsLayoutBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(){
            val model=items[position]
            binding.image.setImageResource(model.image)
            binding.title.text=model.title
            binding.description.text=model.description
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter.ItemViewHolder {
        val itemView=ItemsLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val i=ItemViewHolder(itemView)
        return i
    }
    override fun onBindViewHolder(i: ItemViewHolder, position: Int) {
        i.bind()

    }

    override fun getItemCount()=items.size



}