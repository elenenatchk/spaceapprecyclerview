package com.example.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val items = mutableListOf<itemsModel>()
    private lateinit var adapter: RecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        set()
        init()
    }

    private fun set () {
        items.add(
            itemsModel(
                R.mipmap.earth,
                "Earth",
                "Earth is the fifth largest planet in our solar system, and it's the only one known for sure to have liquid water on its surface."
            )
        )
        items.add(
            itemsModel(
                R.mipmap.mars,
                "Mars",
                "Mars is also known as the 'Red Planet' because, well, it's red! Mars is the second smallest planet in the solar system after Mercury"
            )
        )
        items.add(
            itemsModel(
                R.mipmap.pluto,
                "Pluto",
                "Pluto has five known moons: Charon , Styx, Nix, Kerberos, and Hydra. Pluto and Charon are sometimes considered a binary system because the barycenter of their orbits does not lie within either body."
            )
        )
        items.add(
            itemsModel(
                R.mipmap.mercury,
                "Mercury",
                "Mercury is the smallest planet in the Solar System and the closest to the Sun. Its orbit around the Sun takes 87.97 Earth days, the shortest of all the Sun's planets"
            )
        )
        items.add(
            itemsModel(
                R.mipmap.uranus,
                "Uranus",
                "Uranus is known as the “sideways planet” because it rotates on its side.Uranus was the first planet found using a telescope."
            )
        )
        items.add(
            itemsModel(
                R.mipmap.venuus,
                "Venus",
                "Venus is the second planet from the Sun. It is named after the Roman goddess of love and beauty."
            )
        )
        items.add(
            itemsModel(
                R.mipmap.jupiter,
                "Jupiter",
                "Jupiter is the fifth planet from the Sun and the largest in the Solar System. It is a gas giant with a mass two and a half times that of all the other planets in the Solar System combined, but less than one-thousandth the mass of the Sun."
            )
        )
        items.add(
            itemsModel(
                R.mipmap.saturn,
                "Saturn",
                "Saturn is the sixth planet from the Sun and the second-largest in the Solar System, after Jupiter. It is a gas giant with an average radius of about nine and a half times that of Earth."
            )
        )


    }
    private fun init() {
        adapter = RecyclerViewAdapter(items)
        binding.Recyclerviewcont.layoutManager = LinearLayoutManager(this)
        binding.Recyclerviewcont.adapter = adapter
    }
}